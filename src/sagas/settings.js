import { takeEvery, all } from 'redux-saga/effects'
import {
  handleEventLinkFound,
  handleEventLinkSearchFinished,
  settingChangesSaga,
  sendFeedbackSaga 
} from './events'
import { EXTERNAL_EVENTS } from '../../../common'

export function * settingsSaga () {
  yield all([
    takeEvery(EXTERNAL_EVENTS.LINK_FOUND, handleEventLinkFound),
    takeEvery(EXTERNAL_EVENTS.LINK_SEARCH_FINISHED, handleEventLinkSearchFinished),
    settingChangesSaga(),
    sendFeedbackSaga()
  ])
}
