import { fork, takeEvery } from 'redux-saga/effects'
import {
    settingChangesSaga,
    handleSettingChangeUseLinkGps,
    handleSettingChanged } from '../settings-change'

import { SETTING_CHANGE_USE_LINK_GPS } from '../../../event-types'
import { SETTINGS, ORDER_FILTER_MODES, storeSetting, informAboutSettingChange, ORDER_SORTING_MODE, WORKING_TIME_STATUS } from '../../../../../features'
import { SETTING_CHANGED } from '../../../../../common'

describe('Setting changes saga', () => {
    it('should handle all relevant action types', () => {
        const expected = [
            fork(takeEvery, SETTING_CHANGE_USE_LINK_GPS, handleSettingChangeUseLinkGps),
            fork(takeEvery, SETTING_CHANGED, handleSettingChanged)
        ]
        const generator = settingChangesSaga()

        const result = generator.next().value
        // Hack here as jest can't match anonymous functions
        expect(JSON.stringify(result.ALL)).toBe(JSON.stringify(expected))
    })

    it('should handle a setting change generically', () => {
        testSuccessfulSettingChange(SETTINGS.ORDERS_FILTER_CONFIG, ORDER_FILTER_MODES.SHOW_FINISHED_ORDERS)
        testSuccessfulSettingChange(SETTINGS.ORDERS_SORTING_CONFIG, ORDER_SORTING_MODE.SORT_BY_RECEPTION_TIME_NEWEST_TOP)
        testSuccessfulSettingChange(SETTINGS.SHUTDOWN_AUTOMATICALLY_LOGOFF_DRIVER, true)
        testSuccessfulSettingChange(SETTINGS.SHUTDOWN_AUTOMATICALLY_SET_WORKING_TIMES_TO_TYPE, WORKING_TIME_STATUS.BREAK)
        testSuccessfulSettingChange(SETTINGS.USE_LINK_GPS_LOCATION, false)
    })

    it('should handle a setting change with wrong or no settingId', () => {
        testEmptyOrWrongSettingChange(null, ORDER_FILTER_MODES.SHOW_ALL_ORDERS)
        testEmptyOrWrongSettingChange('', ORDER_FILTER_MODES.SHOW_ALL_ORDERS)
        testEmptyOrWrongSettingChange({}, ORDER_FILTER_MODES.SHOW_ALL_ORDERS)
        testEmptyOrWrongSettingChange('IamAnUnknownSettingId', ORDER_FILTER_MODES.SHOW_ALL_ORDERS)
        testEmptyOrWrongSettingChange(1234, ORDER_FILTER_MODES.SHOW_ALL_ORDERS)
    })
})

function testSuccessfulSettingChange (settingId, settingValue) {
    const generator = handleSettingChanged({
        settingId,
        settingValue
    })

    const storeSettingResult = generator.next().value.PUT
    expect(storeSettingResult.action).toEqual(
        storeSetting(settingId, settingValue))

    // inform about interested saga about the setting change
    const informAboutChangeResult = generator.next().value.PUT
    expect(informAboutChangeResult.action).toEqual(
        informAboutSettingChange(settingId, settingValue))

    expect(generator.next().done).toBeTruthy()
}

function testEmptyOrWrongSettingChange (settingId, settingValue) {
    const generator = handleSettingChanged({
        settingId,
        settingValue
    })

    expect(generator.next().done).toBeTruthy()
}
