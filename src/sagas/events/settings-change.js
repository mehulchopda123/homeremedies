import { put, all, takeEvery } from 'redux-saga/effects'
import { SETTING_CHANGE_USE_LINK_GPS } from '../../event-types'
import {
    storeSetting,
    SETTING_FEATURES_ARRAY,
    informAboutSettingChange } from '../../../../features'
import {
    SETTING_CHANGED,
    startLinkGpsFeed,
    stopLinkGpsFeed } from '../../../../common'

/**
 * Event that is triggered when the user changes the link gps setting.
 * @param {Object} event
 */
export function * handleSettingChangeUseLinkGps (event) {
    const { settingValue: useLinkGpsSource } = event
    console.log('Location: Changing setting "use link gps" to ' + useLinkGpsSource)

    if (useLinkGpsSource) {
        yield put(startLinkGpsFeed())
    }
    else {
        yield put(stopLinkGpsFeed())
    }
}

export function * handleSettingChanged (event) {
    const { settingId, settingValue } = event
    if (settingId && SETTING_FEATURES_ARRAY.includes(settingId)) {
        console.log('Settings: Setting ' + settingId + ' changed to ' + settingValue)
        yield put(storeSetting(settingId, settingValue))
        yield put(informAboutSettingChange(settingId, settingValue))
    }
    else {
        console.warn('Settings: Wrong or missing settingId ' + settingId)
    }
}

export function * settingChangesSaga () {
    yield all([
        takeEvery(SETTING_CHANGE_USE_LINK_GPS, handleSettingChangeUseLinkGps),
        takeEvery(SETTING_CHANGED, handleSettingChanged)
    ])
}
