import { select, call, put, takeEvery, all } from 'redux-saga/effects'
import { addTemporaryToast, SEND_FEEDBACK } from '../../../../common'
import { buildToast } from '../../../../platform'
import DeviceInfo from 'react-native-device-info'
import moment from 'moment'
import { selectFeedback } from '../../selectors/select-feedback'

const FEEDBACK_WEBHOOK_URL = 'https://outlook.office.com/webhook/7b28a830-5750-4da6-ad00-b67c6e20bffb@e648a634-1151-497c-9797-0f975bddecc0/IncomingWebhook/d40d20a89e684e95998d54e1f04af936/afd4a0ff-e924-4767-8648-1ed6d1e23668'

export function * handleSendFeedback ({ text, sender }) {
    if (text) {
        console.log('Feedback: Sending feedback ' + text)
        const state = yield select((state) => state)
        const feedback = yield select(selectFeedback)
        const batteryLevel = yield call(DeviceInfo.getBatteryLevel)
        const isBatteryCharging = yield call(DeviceInfo.isBatteryCharging)
        const isLocationEnabled = yield call(DeviceInfo.isLocationEnabled)

        const {
            device,
            activation,
            appState,
            features,
            location,
            trip,
            workingTimes,
            textMessages,
            binaryMessages,
            driver,
            driverList,
            orderList,
            odometer } = state

        const body = JSON.stringify({
            contentType: 'MessageCard',
            title: 'Feedback from ' + feedback.sender,
            subtitle: 'Subtitle test',
            text,
            // max 10 allowed!
            sections: [
                {
                    activityTitle: 'Meta data',
                    facts: [
                        {
                            name: 'Feedback created at',
                            value: moment().toLocaleString()
                        },
                        {
                            name: 'Locale',
                            value: moment().locale()
                        },
                        {
                            name: 'App version',
                            value: DeviceInfo.getVersion()
                        },
                        {
                            name: 'Device type',
                            value: device.type
                        },
                        {
                            name: 'Orientation',
                            value: device.orientation
                        },
                        {
                            name: 'Battery level',
                            value: batteryLevel * 100 + ' %'
                        },
                        {
                            name: 'Charging',
                            value: isBatteryCharging
                        },
                        {
                            name: 'Number of unsent messages',
                            value: binaryMessages.length
                        },
                        {
                            name: 'Location enabled',
                            value: isLocationEnabled
                        },
                        {
                            name: 'Odometer',
                            value: odometer.valueMeter + ' meter'
                        }
                    ]
                },
                createStateSection({ activation, appState }, 'Activation & App state'),
                createStateSection(features, 'Features'),
                createStateSection(trip, 'Trip'),
                createStateSection(location, 'Location'),
                createStateSection(workingTimes, 'WorkingTimes'),
                createStateSection(textMessages, 'TextMessages'),
                createStateSection(driver, 'Driver'),
                createStateSection(driverList, 'DriverList'),
                createStateSection(orderList, 'OrderList')
            ]
        })

        try {
            const response = yield call(fetch, FEEDBACK_WEBHOOK_URL, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body
            })
            if (response.status === 200) {
                console.log('Feedback sent')
                const toast = buildToast('Your feedback was submitted.', { title: 'Thank you!' })
                yield put(addTemporaryToast(toast))
            }
        }
        catch (error) {
            console.error('Feedback NOT sent', error)
            const toast = buildToast('Your feedback could not be submitted. Please try again later.', { title: 'Oops', isMultiLine: true })
            yield put(addTemporaryToast(toast))
        }
    }
}

function createStateSection (state, name) {
    let subtitle = ''
    try {
        subtitle = 'Number of entries: ' + Object.keys(state).length
    }
    catch (e) {
        console.warn('Could not create subtitle for ' + name + ' - ' + e)
    }

    let stateString = stringifyState(state)
    if (stateString.length > 3000) {
        stateString = 'State string too long ' + name + ', ' + stateString.length
        console.info(stateString)
    }

    return {
        activityTitle: name,
        activitySubtitle: subtitle,
        text: stateString
    }
}

function stringifyState (state) {
    return '<pre>' + JSON.stringify(state, null, 4) + '</pre>'
}

export function * sendFeedbackSaga () {
    yield all([
        takeEvery(SEND_FEEDBACK, handleSendFeedback)
    ])
}
