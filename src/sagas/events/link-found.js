import { put } from 'redux-saga/effects'
import { linkFound } from '../../actions'

/**
 * Event that is triggered if a new link box was found.
 * @param {Object} event
 */
export function * handleEventLinkFound (event) {
    const linkObject = event.payload
    console.log('LINK box found.')
    yield put(linkFound(linkObject))
}
