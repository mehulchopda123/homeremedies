import { put } from 'redux-saga/effects'
import { updateIsLinkSearchRunning } from '../../actions/'

/**
 * Event that is triggered if the link search was finished.
 * @param {Object} event
 */
export function * handleEventLinkSearchFinished (/* event */) {
    console.log('LINK search finished.')
    yield put(updateIsLinkSearchRunning(false))
}
