import { fork, takeEvery } from 'redux-saga/effects'
import {
    handleEventLinkFound,
    handleEventLinkSearchFinished,
    settingChangesSaga,
    sendFeedbackSaga } from '../events'
import { settingsSaga } from '..'
import { UPDATE_LINK_SEARCH_IS_RUNNING } from '../../action-types'
import { EXTERNAL_EVENTS } from '../../../../common'

describe('Settings saga', () => {
    it('should handle all relevant action types', () => {
        const expected = [
            fork(takeEvery, EXTERNAL_EVENTS.LINK_FOUND, handleEventLinkFound),
            fork(takeEvery, EXTERNAL_EVENTS.LINK_SEARCH_FINISHED, handleEventLinkSearchFinished),
            settingChangesSaga(),
            sendFeedbackSaga()
        ]
        const generator = settingsSaga()

        const result = generator.next().value
        // Hack here as jest can't match anonymous functions
        expect(JSON.stringify(result.ALL)).toBe(JSON.stringify(expected))
    })

    it('should perform a link found action', () => {
        const expected = fork(takeEvery, EXTERNAL_EVENTS.LINK_FOUND, handleEventLinkFound)
        const generator = settingsSaga()

        const result = generator.next(EXTERNAL_EVENTS.LINK_FOUND).value

        // Hack here as jest can't match anonymous functions
        expect(JSON.stringify(result.ALL)).toContain(JSON.stringify(expected))
    })

    it('should update a link box in the results', () => {
        const linkObject = {
            btAddress: '1234',
            btName: 'testLink1',
            isConnected: true
        }

        const generator = handleEventLinkFound({ payload: linkObject })

        const result = generator.next().value.PUT
        expect(result).toBeDefined()
        expect(result.action).toBeDefined()

        const link = result.action.linkObject
        expect(link.btAddress).toBe(linkObject.btAddress)
        expect(link.btName).toBe(linkObject.btName)
        expect(link.isConnected).toBeTruthy()

        expect(generator.next().done).toBeTruthy()
    })

    it('should consume a link search finished event', () => {
        const expected = fork(takeEvery, EXTERNAL_EVENTS.LINK_SEARCH_FINISHED, handleEventLinkSearchFinished)
        const generator = settingsSaga()

        const result = generator.next(EXTERNAL_EVENTS.LINK_SEARCH_FINISHED).value

        // Hack here as jest can't match anonymous functions
        expect(JSON.stringify(result.ALL)).toContain(JSON.stringify(expected))
    })

    it('should know when a link search was finished', () => {
        const generator = handleEventLinkSearchFinished({})

        const result = generator.next().value.PUT
        expect(result).toBeDefined()
        expect(result.action).toBeDefined()
        expect(result.action.type).toBe(UPDATE_LINK_SEARCH_IS_RUNNING)

        expect(generator.next().done).toBeTruthy()
    })
})

