import React from 'react';
import {View, FlatList, ScrollView} from 'react-native';

import { ListItem, Text, Body, Thumbnail} from 'native-base';

const ListComponent = ({dataSource, handleClick}) => {
  return (
        <FlatList
          data={dataSource}
          renderItem={({item, i}) => {
            const regex = /(\n)/gi; //  const regex = /(<([^>]+)>)/gi;
            const title = item?.title;
            const description = item?.post_content?.replace(regex, ' ');
            const image = item?.image;

            return (
              <ListItem
                noIndent
                button={true}
                style={{fontSize: 14}}
                onPress={() => handleClick(title, description, image)}>
                <Body
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContet: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Thumbnail
                    square
                    large
                    source={{
                      uri: '' + image,
                    }}
                    style={{height: 35, width: 35}}
                  />
                  <Text style={{fontSize: 18}}>{title}</Text>
                </Body>
              </ListItem>
            );
          }}
          keyExtractor={({id}, index) => id}
        />
  );
};

export default ListComponent;
