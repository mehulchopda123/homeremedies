import React from 'react';
import {StyleSheet, Image, View, Dimensions} from 'react-native';

const BackgroundImageComponent = ({imageSource}) => {
  return (
    <View style={{flex: 1}}>
      <Image ource={imageSource} style={styles.backgroundImage} />
    </View>
  );
};
let ScreenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    //  position: 'absolute',

    zIndex: -1,
    backgroundColor: 'blue',
  },
});
export default BackgroundImageComponent;
