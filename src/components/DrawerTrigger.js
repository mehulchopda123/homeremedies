import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import {Icon} from 'native-base';

// withNavigation allows components to dispatch navigation actions
import {withNavigation} from 'react-navigation';

// DrawerActions is a specific type of navigation dispatcher
import {DrawerActions} from 'react-navigation-drawer';

class DrawerTrigger extends Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.trigger}
        onPress={() => {
          this.props.navigation.dispatch(DrawerActions.openDrawer());
        }}>
        <Icon
          type="FontAwesome"
          name="align-justify"
          style={styles.hamburgerIcon}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  trigger: {
    marginLeft: 5,
  },
  hamburgerIcon: {fontSize: 30, color: '#ffffff'},
});

export default withNavigation(DrawerTrigger);
