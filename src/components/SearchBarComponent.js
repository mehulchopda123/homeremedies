import React, {Component} from 'react';
import {View} from 'react-native';
import {Text, Button, Icon, Item, Input} from 'native-base';
import translate from '../utils/language.utils.js';

const SearchBarComponent = ({searchValue, searchFilterFunction}) => {
  return (
    <View style={{flex: 1}}>
      <Item style={{flex: 1}}>
        <Icon name="ios-search" />
        <Input
          placeholder={translate('search')}
          value={searchValue}
          onChangeText={text => searchFilterFunction(text)}
        />
        <Icon name="ios-people" />
      </Item>
      <Button transparent>
        <Text>Search</Text>
      </Button>
    </View>
  );
};

export default SearchBarComponent;
