import React, {useState} from 'react';
import {Container, Content, Item, Input, Icon, Button, Text} from 'native-base';
import {View} from 'react-native';

const InputForm = ({
  onHeightChange,
  onWeightChange,
  onCalculate,
  onClear,
  textHeight,
  textWeight,
}) => {
  return (
    <Container>
      <Content>
        <Item regular>
          <Input
            placeholder="Height in cm"
            onChangeText={text => {
              onHeightChange(text);
            }}
            keyboardType="numeric"
            value={textHeight.toString()}
          />
        </Item>
        <Item regular style={{marginTop: 15}}>
          <Input
            placeholder="Weight in kg"
            onChangeText={text => {
              onWeightChange(text);
            }}
            keyboardType="numeric"
            value={textWeight.toString()}
          />
        </Item>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            marginTop: 30,
            justifyContent: 'center',
          }}>
          <Button
            success
            onPress={onCalculate}
            style={{
              borderRadius: 120,
              width: 120,
              height: 120,
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 30,
            }}>
            <Text>Calculate</Text>
          </Button>
          <Button
            danger
            onPress={onClear}
            style={{
              borderRadius: 120,
              width: 120,
              height: 120,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text>Clear</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
};

export default InputForm;
