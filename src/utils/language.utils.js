import i18n from '../utils/i18n.js';
// You can import i18n-js as well if you don't want the app to set default locale from the device locale.
import * as RNLocalize from 'react-native-localize';
import en from '../config/language/en';
import hi from '../config/language/hi';
import de from '../config/language/de';

//i18n.fallbacks = false; // If an English translation is not available in en.js, it will look inside hi.js
//i18n.missingBehaviour = 'guess'; // It will convert HOME_noteTitle to "HOME note title" if the value of HOME_noteTitle doesn't exist in any of the translation files.
//i18n.defaultLocale = 'en'; // If the current locale in device is not en or hi
//i18n.locale = 'en'; // If we do not want the framework to use the phone's locale by default

const locales = RNLocalize.getLocales();

if (Array.isArray(locales)) {
  i18n.locale = locales[0].languageTag;
}

i18n.translations = {
  hi,
  en,
  de,
};

export const setLocale = locale => {
  i18n.locale = locale;
};

i18n.locale = 'en'; //disable this when you want to add other translation

export const getCurrentLocale = () => i18n.locale; // It will be used to define intial language state in reducer.

/* translateHeaderText:
  screenProps => coming from react-navigation (defined in app.container.js)
  langKey => will be passed from the routes file depending on the screen.(We will explain the usage later int the coming topics)
 */
export const translateHeaderText = langKey => ({screenProps}) => {
  const title = i18n.translate(langKey, screenProps.language);
  return {title};
};

export default i18n.translate.bind(i18n);
