import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';

i18n.defaultLocale = 'en';
i18n.locale = 'en';
i18n.fallbacks = true;

export default i18n;
