import React from 'react';
import {Platform, Image} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
//import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';
import RemediesScreen from '../screens/RemediesScreen';
import HerbsScreen from '../screens/HerbsScreen';
//import FruitsScreen from '../screens/FruitsScreen';
import RemediesDetailsScreen from '../screens/RemediesDetailsScreen';
import PrivacyPolicyScreen from '../screens/PrivacyPolicyScreen';
import BmiCalcScreen from '../screens/BmiCalcScreen';
import Icon from 'react-native-vector-icons/FontAwesome';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import {
  Container,
  Content,
  Header,
  Body,
  Left,
  Right,
  StyleProvider,
} from 'native-base';

const config = Platform.select({
  web: {headerMode: 'screen'},
  default: {},
});

const HomeRemedies = createStackNavigator(
  {
    RemediesScreen: {screen: RemediesScreen},
    RemediesDetails: {screen: RemediesDetailsScreen},
  },
  {
    initialRouteName: 'RemediesScreen',
  },
);

HomeRemedies.navigationOptions = {
  drawerLabel: 'Diseases & Remedies',
  drawerIcon: ({tintColor}) => (
    <Icon name="ambulance" size={20} color={tintColor} />
  ),
};

HomeRemedies.path = '';

const Herbs = createStackNavigator(
  {
    HerbsScreen: {screen: HerbsScreen},
    RemediesDetails: {screen: RemediesDetailsScreen},
  },
  {
    initialRouteName: 'HerbsScreen',
  },
);

Herbs.navigationOptions = {
  drawerLabel: 'Natural Herbs',
  drawerIcon: ({tintColor}) => <Icon name="leaf" size={20} color={tintColor} />,
};

Herbs.path = '';

const BmiCalc = createStackNavigator(
  {
    BmiCalcScreen: {screen: BmiCalcScreen},
  },
  {
    initialRouteName: 'BmiCalcScreen',
  },
);
BmiCalc.navigationOptions = {
  drawerLabel: 'Calcuate BMI',
  drawerIcon: ({tintColor}) => (
    <Icon name="calculator" size={20} color={tintColor} />
  ),
};

BmiCalc.path = '';

const PrivacyPolicy = createStackNavigator(
  {
    PrivacyPolicyScreen: {screen: PrivacyPolicyScreen},
  },
  {
    initialRouteName: 'PrivacyPolicyScreen',
  },
);
PrivacyPolicy.navigationOptions = {
  drawerLabel: 'Privacy Policy',
  drawerIcon: ({tintColor}) => <Icon name="book" size={20} color={tintColor} />,
};

PrivacyPolicy.path = '';

// const Screen3Stack = createStackNavigator(
//   {
//     FruitsScreen: {screen: FruitsScreen},
//     RemediesDetails: {screen: RemediesDetailsScreen},
//   },
//   {
//     initialRouteName: 'FruitsScreen',
//   },
// );

// Screen3Stack.navigationOptions = {
//   tabBarLabel: 'Fruits',
//   tabBarIcon: ({focused}) => <Icon name="apple" size={30} color="#839e2e" />,
// };

// Screen3Stack.path = '';

// const tabNavigator = createBottomTabNavigator({
//   HomeRemedies,
//   Herbs,
// });
const CustomDrawerContentComponent = (props) => (
  
    <StyleProvider style={getTheme(material)}>
        <Container>
          <Header style={styles.drawerHeader} androidStatusBarColor="#000000">
            <Body style={styles.drawerBody}>
              <Image
                style={styles.drawerImage}
                source={require('../assets/images/icon.png')}
              />
            </Body>
          </Header>
          <Content>
            <DrawerItems {...props} />
          </Content>
        </Container>
      </StyleProvider>
);

const drawerNavigator = createDrawerNavigator(
  {
    HomeRemedies,
    Herbs,
    BmiCalc,
    PrivacyPolicy,
  },
  {
    contentComponent: props => <CustomDrawerContentComponent {...props} />,
    contentOptions: {
      activeTintColor: '#839e2e',
      inactiveTintColor: '#030303',
      itemsContainerStyle: {
        marginVertical: 0,
      },
      labelStyle: {fontSize: 16},
      iconContainerStyle: {
        opacity: 1,
      },
    },
  },
);

drawerNavigator.path = '';

const styles = {
  drawerBody: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  drawerImage: {
    height: 150,
    width: 150,
  },
  drawerLinkIcons: {
    height: 24,
    width: 24,
  },
  drawerHeader: {
    height: 170,
    backgroundColor: 'white',
  },
  icon: {
    tintColor: '#F50057',
  },
};
export default drawerNavigator;
