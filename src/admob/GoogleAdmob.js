import { InterstitialAd, TestIds } from '@react-native-firebase/admob';

export const AdUnitId = {
  android: __DEV__ ? TestIds.BANNER : 'ca-app-pub-5952490106741256/7989274962',
  ios: __DEV__ ? TestIds.BANNER : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy',
};

export const interstitialAdUnitId = {
  android: __DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-5952490106741256/3532139956',
  ios: __DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy',
};

export const interstitialAds = InterstitialAd.createForAdRequest(interstitialAdUnitId.android);



