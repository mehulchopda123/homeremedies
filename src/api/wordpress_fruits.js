import axios from 'axios';
import translate from '../utils/language.utils.js';
import i18n from '../utils/i18n.js';

export default axios.create({
  baseURL:
    'https://remedies.playforearns.com/wp-json/api/v2/posts?&post_type=fruits&lang=' +
    i18n.locale,
  // headers:{
  //     Authorization: 'Bearer lsgLnoWeex2D__Bqgs2Dvz4BAt48mhvFKKwenlDte12-pwgafIOseA7gktyUsnURNhc2J0aK0uOSKEoYX55KXSdJ2SwiBG_3jyclp0w5J8F0WSv71qQCPr9hcgq8XXYx'
  // }
});
