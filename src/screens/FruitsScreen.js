import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {ActivityIndicator, View, Alert} from 'react-native';

import {connect} from 'react-redux';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Left,
  Body,
  Right,
  Title,
  Button,
  Icon,
  StyleProvider,
  Item,
  Input,
  Thumbnail,
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import translate from '../utils/language.utils.js';
import fruitsApi from '../api/wordpress_fruits';
import ListComponent from '../components/ListComponent';
import SearchBarComponent from '../components/SearchBarComponent';
import {BannerAd, BannerAdSize} from '@react-native-firebase/admob';
import {AdUnitId} from '../admob/GoogleAdmob';

class FruitsScreen extends Component {
  static propTypes = {
    todos: PropTypes.array,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      active: false,
      isLoading: true,
      isAdsLoaded: false,
      isAdsRequested: false,
      dataSource: [],
      error: '',
    };
    this.searchArrayholder = [];
  }

  componentDidMount() {
    this.getPosts();
    const eventListener = interstitialAds.onAdEvent(type => {
      if (type === AdEventType.LOADED) {
        this.setState({ isAdsLoaded: true })
        interstitialAds.show();
      }
      if (type === AdEventType.CLOSED) {
        this.setState({ isAdsLoaded: false })
      }
    });
    interstitialAds.load()
  }

  componentWillUnmount() {
    if (this.state.isAdsLoaded) {
      interstitialAds.show();
  }

  getPosts = async () => {
    const res = await fruitsApi.get('', {});
    const datas = res.data;
    this.setState({
      isLoading: false,
      dataSource: datas,
    });
    this.searchArrayholder = datas;
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left>
              <Button transparent>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>Natural Fruits</Title>
            </Body>
            <Right />
          </Header>
          <Header searchBar rounded autoCorrect={false}>
            <SearchBarComponent
              searchValue={this.state.value}
              searchFilterFunction={this.searchFilterFunction}
            />
          </Header>
            <ListComponent
              dataSource={this.state.dataSource}
              handleClick={this.handleClick}
            />
        </Container>
      </StyleProvider>
    );
  }

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });
    const newData = this.searchArrayholder.filter(item => {
      const itemData = `${item?.title.toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({dataSource: newData});
  };

  handleClick = (title, description, image) => {
    this.props.navigation.navigate('RemediesDetails', {
      title,
      description,
      image,
    });
  };
}

FruitsScreen.navigationOptions = {
  header: null,
};

function mapStateToProps(state) {
  return {
    todos: state.todos,
  };
}

export default connect(mapStateToProps)(FruitsScreen);
