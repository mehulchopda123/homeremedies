import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {Image} from 'react-native';
import {WebView} from 'react-native-webview';
import {connect} from 'react-redux';
import {
  Container,
  Header,
  Content,
  Card,
  View,
  Body,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Title,
  Right,
  StyleProvider,
} from 'native-base';
import translate from '../utils/language.utils.js';
import DrawerTrigger from '../components/DrawerTrigger';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';

class PrivacyPolicyScreen extends Component {
  static propTypes = {
    todos: PropTypes.array,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }

  render() {
    const {navigation} = this.props;

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left style={{flex: 1}}>
              <DrawerTrigger />
            </Left>
            <Body style={{flex: 4}}>
              <Title>Privacy Policy</Title>
            </Body>
            <Right />
          </Header>

          <Content padder contentContainerStyle={{flex: 1}}>
            <WebView
              originWhitelist={['*']}
              source={{
                html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
              </head><body>
              <div>A lot of companies say they care about your privacy — we say it and mean it!</div>

<div>This document is our Privacy Policy, which describes what information we collect from you and how we use it. This is important; we hope you will take time to read it carefully.</div>

<div>We use Google Admob SDK for advertisements in our applications.</div>

<h5>User Provided Information & Use of your that Information</h5>
This Application does not require any persional information.

<h5>Automatically Collected Information</h5>
<div>This app does not automatically collect any persional information.</div>

<h5>CONFIDENTIALITY</h5>
<div>We do not collect sensitive personal data or information of users like password of your e mail account, financial information such as Bank Account details or Credit Card or Debit Card or other payment related details.</div>

<h5>NOTIFICATION OF MODIFICATIONS AND CHANGES TO THE T&C AND PRIVACY POLICY</h5>
<div>We reserve the right to change the Terms and Privacy Policy from time to time as it sees fit and your continued use of the app will signify your acceptance of any adjustment to these terms. You are therefore advised to re-read the Terms and Privacy Policy on a regular basis. Should it be that you do not accept any of the modifications or amendments to the Terms and Privacy Policy, you may terminate your use of this application immediately.
              </div>
              </body></html>`,
              }}
              styles={{
                flex: 1,
              }}
            />
          </Content>
        </Container>
      </StyleProvider>
    );
  }
  navigateBack = () => {
    this.props.navigation.goBack();
  };
}

PrivacyPolicyScreen.navigationOptions = {
  header: null,
};

function mapStateToProps(state) {
  return {
    todos: state.todos,
  };
}

export default connect(mapStateToProps)(PrivacyPolicyScreen);
