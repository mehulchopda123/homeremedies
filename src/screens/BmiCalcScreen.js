import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {Image, Keyboard} from 'react-native';
import {WebView} from 'react-native-webview';
import {connect} from 'react-redux';
import {
  Container,
  Header,
  Content,
  Card,
  View,
  Body,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Title,
  Right,
  StyleProvider,
} from 'native-base';
import translate from '../utils/language.utils.js';
import DrawerTrigger from '../components/DrawerTrigger';
import InputForm from '../components/InputForm';
import SegmentedProgressBar from 'react-native-segmented-progress-bar';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import {BannerAd, BannerAdSize, InterstitialAd, AdEventType} from '@react-native-firebase/admob';
import {AdUnitId, interstitialAds} from '../admob/GoogleAdmob';

class BmiCalcScreen extends Component {
  static propTypes = {
    todos: PropTypes.array,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      weight: 0,
      resultNumber: 0,
      resultText: '',
      isDataEntered: false,
      isAdsLoaded: false,
      isAdsRequested: false,
    };
    this.textInput = React.createRef();
  }

  componentDidMount() {
    const eventListener = interstitialAds.onAdEvent(type => {
      if (type === AdEventType.LOADED) {
        this.setState({ isAdsLoaded: true })
        interstitialAds.show();
      }
      if (type === AdEventType.CLOSED) {
        this.setState({ isAdsLoaded: false })
      }
    });
    interstitialAds.load()
  }

  componentWillUnmount() {
    if (this.state.isAdsLoaded) {
      interstitialAds.show();
    }
  }

  render() {
    const {navigation} = this.props;

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left style={{flex: 1}}>
              <DrawerTrigger />
            </Left>
            <Body style={{flex: 4}}>
              <Title>BMI Calculator</Title>
            </Body>
            <Right />
          </Header>

          <Content padder>
            <Text style={{fontSize: 14, marginBottom: 30}}>
              Body Mass Index is a simple calculation using a person’s height
              and weight.
            </Text>
            <View style={{height: 310}}>
              <InputForm
                onHeightChange={this.onHeightChange}
                onWeightChange={this.onWeightChange}
                onCalculate={this.onCalculate}
                onClear={this.onClear}
                textHeight={this.state.height}
                textWeight={this.state.weight}
              />
            </View>
            {this.state.isDataEntered ? (
              <View>
                <Text>
                  BMI is : {this.state.resultNumber} ({this.state.resultText})
                </Text>
                <SegmentedProgressBar
                  showSeparatorValue
                  values={[0, 18.5, 24.9, 29.9, 40]}
                  colors={['grey', 'green', 'orange', 'red']}
                  labels={['underweight', 'normal', 'overweight', 'obese']}
                  position={this.state.resultNumber}
                  height={20}
                />
              </View>
            ) : null}
          </Content>
          <View>
            <BannerAd
              unitId={AdUnitId.android}
              size={BannerAdSize.SMART_BANNER}
            />
          </View>
        </Container>
      </StyleProvider>
    );
  }
  navigateBack = () => {
    this.props.navigation.goBack();
  };
  onHeightChange = height => {
    this.setState({height: height});
  };
  onWeightChange = weight => {
    this.setState({weight: weight});
  };

  onCalculate = () => {
    let imc = this.state.weight / (this.state.height * 0.01) ** 2;
    this.setState({
      resultNumber: imc.toFixed(2),
    });

    if (this.state.resultNumber !== undefined && Number.isNaN(imc) === false) {
      this.setState({
        isDataEntered: true,
      });
    }
    if (imc < 18.5) {
      this.setState({resultText: 'Underweight'});
    } else if (imc > 18.5 && imc < 24.9) {
      this.setState({resultText: 'Normal Weight'});
    } else if (imc >= 25 && imc < 29.9) {
      this.setState({resultText: 'Overweight'});
    } else {
      this.setState({resultText: 'Obesity'});
    }
    Keyboard.dismiss();
  };

  onClear = () => {
    this.setState({
      height: 0,
      weight: 0,
      resultNumber: 0,
      resultText: '',
      isDataEntered: false,
    });
    Keyboard.dismiss();
  };
}

BmiCalcScreen.navigationOptions = {
  header: null,
};

function mapStateToProps(state) {
  return {
    todos: state.todos,
  };
}

export default connect(mapStateToProps)(BmiCalcScreen);
