import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {Image} from 'react-native';
import {WebView} from 'react-native-webview';
import {connect} from 'react-redux';
import {
  Container,
  Header,
  Content,
  Card,
  View,
  Body,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Title,
  Right,
  StyleProvider,
} from 'native-base';
import translate from '../utils/language.utils.js';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import {BannerAd, BannerAdSize, InterstitialAd, AdEventType} from '@react-native-firebase/admob';
import {AdUnitId, interstitialAds} from '../admob/GoogleAdmob';

class RemediesDetailsScreen extends Component {
  static propTypes = {
    todos: PropTypes.array,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      active: false,
      isAdsLoaded: false,
      isAdsRequested: false
    };
  }

  componentDidMount() {
    const eventListener = interstitialAds.onAdEvent(type => {
      if (type === AdEventType.LOADED) {
        this.setState({ isAdsLoaded: true })
        interstitialAds.show();
      }
      if (type === AdEventType.CLOSED) {
        this.setState({ isAdsLoaded: false })
      }
    });
    interstitialAds.load()
  }

  componentWillUnmount() {
    if (this.state.isAdsLoaded) {
      interstitialAds.show();
    }
  }

  render() {
    const {navigation} = this.props;
    const title = JSON.stringify(navigation.getParam('title', null)).replace(
      /\"/g,
      '',
    );
    const description = JSON.stringify(navigation.getParam('description', null))
      ?.slice(1, -1)
      .replace(/\\/g, '');
    const image = JSON.stringify(navigation.getParam('image', null)).replace(
      /\"/g,
      '',
    );

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left style={{flex: 1}}>
              <Button transparent onPress={this.navigateBack}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body style={{flex: 4}}>
              <Title>{title}</Title>
            </Body>
            <Right />
          </Header>

          <Content padder contentContainerStyle={{flex: 1}}>
            <WebView
              originWhitelist={['*']}
              source={{
                html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
              </head><body>${description}</body></html>`,
              }}
              styles={{
                flex: 1,
              }}
            />
          </Content>
          <View>
            <BannerAd
              unitId={AdUnitId.android}
              size={BannerAdSize.SMART_BANNER}
            />
          </View>
        </Container>
      </StyleProvider>
    );
  }
  navigateBack = () => {
    this.props.navigation.goBack();
  };
}

RemediesDetailsScreen.navigationOptions = {
  header: null,
};

function mapStateToProps(state) {
  return {
    todos: state.todos,
  };
}

export default connect(mapStateToProps)(RemediesDetailsScreen);
