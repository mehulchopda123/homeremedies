import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {ActivityIndicator, View, Alert} from 'react-native';

import {connect} from 'react-redux';
import {
  Container,
  Header,
  List,
  ListItem,
  Text,
  Left,
  Body,
  Right,
  Title,
  Button,
  Icon,
  StyleProvider,
  Item,
  Input,
  Thumbnail,
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import translate from '../utils/language.utils.js';
import wordpress from '../api/wordpress';
import ListComponent from '../components/ListComponent';
import SearchBarComponent from '../components/SearchBarComponent';
import DrawerTrigger from '../components/DrawerTrigger';
import {BannerAd, BannerAdSize, InterstitialAd, AdEventType} from '@react-native-firebase/admob';
import {AdUnitId, interstitialAds} from '../admob/GoogleAdmob';

class RemediesScreen extends Component {
  static propTypes = {
    todos: PropTypes.array,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isAdsLoaded: false,
      isAdsRequested: false,
      active: false,
      isLoading: true,
      dataSource: [],
      error: '',
    };
    this.searchArrayholder = [];
  }

  componentDidMount() {
    this.getPosts();
    const eventListener = interstitialAds.onAdEvent(type => {
      if (type === AdEventType.LOADED) {
        this.setState({ isAdsLoaded: true })
        interstitialAds.show();
      }
      if (type === AdEventType.CLOSED) {
        this.setState({ isAdsLoaded: false })
      }
    });
    interstitialAds.load()
  }

  componentWillUnmount() {
    if (this.state.isAdsLoaded) {
      interstitialAds.show();
    }
  }

  getPosts = async () => {
    const res = await wordpress.get('', {});
    const datas = res.data;
    this.setState({
      isLoading: false,
      dataSource: datas,
    });
    this.searchArrayholder = datas;
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Header>
            <Left style={{flex: 1}}>
              <DrawerTrigger />
            </Left>
            <Body style={{flex: 4}}>
              <Title>Diseases & Remedies</Title>
            </Body>
            <Right />
          </Header>
          <Header searchBar rounded autoCorrect={false}>
            <SearchBarComponent
              searchValue={this.state.value}
              searchFilterFunction={this.searchFilterFunction}
            />
          </Header>
          <ListComponent
              dataSource={this.state.dataSource}
              handleClick={this.handleClick}
            />
          <View>
            <BannerAd
              unitId={AdUnitId.android}
              size={BannerAdSize.SMART_BANNER}
            />
          </View>
        </Container>
      </StyleProvider>
    );
  }

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });
    const newData = this.searchArrayholder.filter(item => {
      const itemData = `${item?.title.toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({dataSource: newData});
  };

  handleClick = (title, description, image) => {
    this.props.navigation.navigate('RemediesDetails', {
      title,
      description,
      image,
    });
  };
}

RemediesScreen.navigationOptions = {
  header: null,
};

function mapStateToProps(state) {
  return {
    todos: state.todos,
  };
}

export default connect(mapStateToProps)(RemediesScreen);
