import {
    LINK_FOUND,
    UPDATE_LINK_SEARCH_IS_RUNNING,
    SET_LOCAL_BT_ADDRESS,
    SET_READ_ALOUD_INCOMING_MESSAGES,
    SET_AUTO_RECONNECT_TO_LINK } from '../../action-types'
import {
    linkFound,
    updateIsLinkSearchRunning,
    setLocalBtAddress,
    setReadAloudTMORs,
    setAutoReconnectToLink } from '..'

describe('Add a new link box from the search', () => {
    it('should create an action to add a link box from the search', () => {
        const linkObject = {
            btAddress: '1234',
            btName: 'testLink1',
            signalStrength: -87
        }
        const expected = {
            type: LINK_FOUND,
            linkObject
        }
        expect(linkFound(linkObject)).toEqual(expected)
    })
})

describe('Indicate that a LINK search is running', () => {
    it('should create an action to indicate that a LINK search is running', () => {
        const isLinkSearchRunning = true
        const expected = {
            type: UPDATE_LINK_SEARCH_IS_RUNNING,
            isLinkSearchRunning
        }
        expect(updateIsLinkSearchRunning(isLinkSearchRunning)).toEqual(expected)
    })
})

describe('Change settings', () => {
    it('should be possible to set the local BT address', () => {
        const localBtAddress = '1234'
        const expected = {
            type: SET_LOCAL_BT_ADDRESS,
            localBtAddress
        }
        expect(setLocalBtAddress(localBtAddress)).toEqual(expected)
    })

    it('should be possible to enable or disable read aloud of incoming messages', () => {
        const enabled = false
        const expected = {
            type: SET_READ_ALOUD_INCOMING_MESSAGES,
            enabled
        }
        expect(setReadAloudTMORs(enabled)).toEqual(expected)
    })

    it('should be possible to enable or disable automatic reconnection to LINK', () => {
        const enabled = false
        const expected = {
            type: SET_AUTO_RECONNECT_TO_LINK,
            enabled
        }
        expect(setAutoReconnectToLink(enabled)).toEqual(expected)
    })
})
