import {
  ADD_TODO,
  DELETE_TODO,
  TOGGLE_TODO
} from '../action-types'

export function addToDo (text = '') {
  return { type: ADD_TODO, text }
}

export function toggleToDo (id) {
  return { type: TOGGLE_TODO, id }
}

export function deleteToDo (id, text) {
  return { type: DELETE_TODO, id, text }
}
