import {combineReducers} from 'redux';
import {reducer as mainReducer} from '../reducer';

/*
 * Combine reducers of all modules, add filter to prevent that all actions are
 * checked by all reducers.
 * NOTE: This is done centrally to prevent circular dependencies.
 */
export default combineReducers({
  ...mainReducer,
});
