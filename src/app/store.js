import createSagaMiddleware from 'redux-saga';
import {createStore, applyMiddleware} from 'redux';
import reducer from './reducers';
import {logger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';
import {AsyncStorage} from 'react-native';

const sagaMiddleware = createSagaMiddleware();

// Middleware: Redux Persist Config
const persistConfig = {
  // Root
  key: 'root',
  // Storage Method (React Native)
  storage: AsyncStorage,
  // Whitelist (Save Specific Reducers)
  whitelist: ['todos'],
  // Blacklist (Don't Save Specific Reducers)
  blacklist: ['settings'],
};

// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, reducer);

const store = createStore(
  persistedReducer,
  applyMiddleware(sagaMiddleware, logger),
);

// Middleware: Redux Persist Persister
const persistor = persistStore(store);
export {persistor};
export default store;
