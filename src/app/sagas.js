/*
 * Setup sagas of all modules
 */
import { all } from 'redux-saga/effects'

import {
  settingsSaga
} from '../.sagas'

export function * rootSaga () {
  yield all([
    settingsSaga()
  ])
}
