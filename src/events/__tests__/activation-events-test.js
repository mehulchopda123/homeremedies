import { Papaya } from '../../../../binary/papaya'
import {
    papayaFeatureRequest,
    pndPairingRequest,
    slaveActivationRequest
} from '../activation'
import {
    PND_PAIRING_REQUEST,
    SLAVE_ACTIVATION_REQUEST
} from '../../event-types'

describe('Activation events test', () => {
    it('should create an event for papaya feature request', () => {
        const expected = {
            type: Papaya.FEATURES_REQUEST_MSG_ID
        }
        expect(papayaFeatureRequest()).toEqual(expected)
    })

    it('should create an event for slave activation request', () => {
        const expected = {
            type: SLAVE_ACTIVATION_REQUEST
        }
        expect(slaveActivationRequest()).toEqual(expected)
    })

    it('should create an event for pnd pairing request', () => {
        const expected = {
            type: PND_PAIRING_REQUEST
        }
        expect(pndPairingRequest()).toEqual(expected)
    })
})
