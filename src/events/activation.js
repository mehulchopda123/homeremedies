import {SLAVE_ACTIVATION_REQUEST, PND_PAIRING_REQUEST} from '../event-types';

export function slaveActivationRequest() {
  return {type: SLAVE_ACTIVATION_REQUEST};
}

export function pndPairingRequest() {
  return {type: PND_PAIRING_REQUEST};
}
