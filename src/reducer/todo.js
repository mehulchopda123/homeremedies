import {ADD_TODO, DELETE_TODO, TOGGLE_TODO} from '../action-types';

export const INITIAL_STATE = [];

const todoReducer = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case ADD_TODO: {
      return [
        ...state,
        {
          id: state.length,
          completed: false,
          text: action.text,
        },
      ];
    }

    case TOGGLE_TODO: {
      return state.map(todo => {
        if (todo.id !== action.id) {
          return todo;
        }
        return {
          ...todo,
          completed: !todo.completed,
        };
      });
    }

    case DELETE_TODO: {
      return state.filter(todo => todo.id !== action.id);
    }

    default:
      return state;
  }
};
export default todoReducer;
