import { default as handleSettings } from '../settings'
import {
    setAutoMarkAsRead,
    setLocalBtAddress,
    setReadAloudTMORs,
    setAutoReconnectToLink } from '../../actions/settings'
import { INITIAL_STATE } from '../settings'

describe('The default reducer', () => {
    it('should return a default state', () => {
        expect(handleSettings()).toEqual({
            localBtAddress: '',
            readAloudTMORs: true,
            autoReconnectToLink: true,
            autoMarkAsRead: true
        })
    })

    it('should default to a provided state', () => {
        expect(handleSettings('testState')).toEqual('testState')
    })
})

describe('Setting local BT address', () => {
    const emptyLocalBtAddress = setLocalBtAddress()
    it('should set default value for empty local BT address', () => {
        expect(handleSettings(INITIAL_STATE, emptyLocalBtAddress)).toEqual({
            localBtAddress: '',
            readAloudTMORs: true,
            autoReconnectToLink: true,
            autoMarkAsRead: true
        })
    })
    const btAddress = '00:00:00:00:00:00'
    const localBtAddress = setLocalBtAddress(btAddress)
    it('should set the local BT address', () => {
        expect(handleSettings(INITIAL_STATE, localBtAddress)).toEqual({
            localBtAddress: btAddress,
            readAloudTMORs: true,
            autoReconnectToLink: true,
            autoMarkAsRead: true
        })
    })
})

describe('Setting read aloud incoming messages', () => {
    const defaultReadAloudTMORs = setReadAloudTMORs()
    it('should set default value read aloud incoming messages', () => {
        expect(handleSettings(INITIAL_STATE, defaultReadAloudTMORs)).toEqual({
            localBtAddress: '',
            readAloudTMORs: true,
            autoReconnectToLink: true,
            autoMarkAsRead: true
        })
    })
    const readAloud = setReadAloudTMORs(false)
    it('should set read aloud incoming messages', () => {
        expect(handleSettings(INITIAL_STATE, readAloud)).toEqual({
            localBtAddress: '',
            readAloudTMORs: false,
            autoReconnectToLink: true,
            autoMarkAsRead: true
        })
    })
})

describe('Setting auto reconnect to LINK', () => {
    const defaultAutoReconnectToLink = setAutoReconnectToLink()
    it('should set default value auto reconnect to link', () => {
        expect(handleSettings(INITIAL_STATE, defaultAutoReconnectToLink)).toEqual({
            localBtAddress: '',
            readAloudTMORs: true,
            autoReconnectToLink: true,
            autoMarkAsRead: true
        })
    })
    const autoReconnect = setAutoReconnectToLink(false)
    it('should set auto reconnect to link', () => {
        expect(handleSettings(INITIAL_STATE, autoReconnect)).toEqual({
            localBtAddress: '',
            readAloudTMORs: true,
            autoReconnectToLink: false,
            autoMarkAsRead: true
        })
    })
})

describe('Toggling auto mark as read setting', () => {
    it('should have a default value', () => {
        const defaultSetting = setAutoMarkAsRead()
        expect(handleSettings({}, defaultSetting)).toEqual({
            autoMarkAsRead: true
        })
    })

    it('should update when given new state', () => {
        const defaultSetting = setAutoMarkAsRead(false)
        expect(handleSettings({}, defaultSetting)).toEqual({
            autoMarkAsRead: false
        })
    })
})
