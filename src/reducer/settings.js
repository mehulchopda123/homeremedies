import {
  SET_AUTO_MARK_AS_READ,
  SET_LOCAL_BT_ADDRESS,
  SET_READ_ALOUD_INCOMING_MESSAGES,
  SET_AUTO_RECONNECT_TO_LINK,
} from '../action-types';;

export const INITIAL_STATE = {
  localBtAddress: '',
  readAloudTMORs: true,
  autoReconnectToLink: true,
  autoMarkAsRead: true,,
};;

const settingsReducer = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case SET_AUTO_MARK_AS_READ: {
      return Object.assign({}, state, {autoMarkAsRead: action.enabled});;
    }
    case SET_LOCAL_BT_ADDRESS: {
      return Object.assign({}, state, {localBtAddress: action.localBtAddress});;
    }
    case SET_READ_ALOUD_INCOMING_MESSAGES: {
      return Object.assign({}, state, {readAloudTMORs: action.enabled});;
    }
    case SET_AUTO_RECONNECT_TO_LINK: {
      return Object.assign({}, state, {autoReconnectToLink: action.enabled});;
    }
    default:
      return state;;
  }
};;
export default settingsReducer;;
