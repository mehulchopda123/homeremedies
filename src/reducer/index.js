import todoReducer from './todo';

export const reducer = {
  todos: todoReducer,
};
